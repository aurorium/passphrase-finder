extern crate bitcoin_wallet;
use bitcoin_wallet::account::Account;
use bitcoin_wallet::account::AccountAddressType;
use bitcoin_wallet::account::MasterAccount;
use bitcoin_wallet::account::Unlocker;
use bitcoin_wallet::mnemonic::Mnemonic;

extern crate bitcoin;
use bitcoin::Network;

fn get_address(mnemonic: &Mnemonic, passphrase: &str) -> String {
    println!("passphrase: {:?}", passphrase);

    let mut master =
        MasterAccount::from_mnemonic(mnemonic, 0, Network::Bitcoin, passphrase, None).unwrap();

    let mut unlocker = Unlocker::new_for_master(&master, passphrase).unwrap();

    let account = Account::new(&mut unlocker, AccountAddressType::P2SHWPKH, 0, 0, 10).unwrap();
    master.add_account(account);

    let seed = master.seed(Network::Bitcoin, passphrase).unwrap();

    let mut passphrase_master =
        MasterAccount::from_seed(&seed, 0, Network::Bitcoin, passphrase).unwrap();

    let mut passphrase_unlocker = Unlocker::new_for_master(&passphrase_master, passphrase).unwrap();

    let passphrase_account = Account::new(
        &mut passphrase_unlocker,
        AccountAddressType::P2SHWPKH,
        0,
        0,
        10,
    )
    .unwrap();
    passphrase_master.add_account(passphrase_account);

    let address = passphrase_master
        .get_mut((0, 0))
        .unwrap()
        .next_key()
        .unwrap()
        .address
        .clone();

    return address.to_string();
}

fn main() {
    const WORDS: &str = "enrich choice sad naive curve angry else treat like reveal pelican taste sunset solve transfer cluster cave winter erase broken thank adult girl memory";
    let mnemonic = Mnemonic::from_str(WORDS).unwrap();

    println!("mnemonic: {:?}", mnemonic.to_string());

    println!("{:?}", get_address(&mnemonic, ""));
    // 3BUkSfvhw1xY5HdPW7Ecxia1j3CqZb76y5
    println!("{:?}", get_address(&mnemonic, "ALAPTAK123"));
    // 3BwQPsdk5BghZYLMPuKq1W9mSVcxxUD4iS
}
